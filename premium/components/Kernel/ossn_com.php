<?php
/**
 * Open Source Social Network
 *
 * @package   Kernel
 * @author    SOFTLAB24 LIMITED <info@softlab24.com>
 * @copyright 2020 SOFTLAB24 LIMITED
 * @license   SOFTLAB24 LIMITED, COMMERCIAL LICENSE v1.0
 * @link      https://www.softlab24.com/
 */
define('KERNEL', ossn_route()->com . 'Kernel/');
/**
 * Kernel check if connection is valid
 *
 * @return boolean
 */
function ossn_kernal_is_established() {
		if(isset($_SERVER['X_SLA24_KERNEL_IGNORE']) && $_SERVER['X_SLA24_KERNEL_IGNORE'] == 1){
				return true;	
		}
		$kernel = new OssnKernel;
		$establish = $kernel->sendAuth();
		if(isset($establish) && isset($establish->data)) {
				if(base64_decode($establish->data) !== 'established') {
						return false;
				}
		}
		return true;
}
/**
 * Kernel Initialize
 *
 * @return void
 */
function ossn_kernal_init() {
		ossn_add_hook('ossn', 'kernel:cred', 'ossn_kernal_auth');
		ossn_register_callback('cache', 'flushed', 'ossn_kernel_cache_flushed');
}
/**
 * Set kernal creds
 *
 * @return object|boolean
 */
function ossn_kernal_creds() {
		global $license;
		if(!is_file(KERNEL . 'license.php')) {
				return false;
		}
		include_once(KERNEL . 'license.php');
		if(isset($license['api_key'])) {
				return (object) $license;
		}
		return false;
}
/**
 * Setup a sdk for kernel
 *
 * @return void
 */
if(!OssnKernel::isCacheLoaded()) {
		$status = ossn_kernal_is_established();
		if($status){
				OssnKernel::setStatus(true);
		} else {
				OssnKernel::setStatus(false);	
				OssnKernel::clearStorage();
		}
		define('KERNEL_STATUS', $status);
} else {
		define('KERNEL_STATUS', true);
}
/**
 * Register the SDK
 *
 * @param string $type Type of SDK
 * @param string $handler Name of handler
 * 
 * @return void
 */
function ossn_register_system_sdk($type, $handler, $pcit = 4001) {
		global $Ossn;
		if(!isset($Ossn->kernelBatch)) {
				$Ossn->kernelBatch = array();
		}
		$Ossn->kernelBatch[$type] = array(
				$type,
				$handler,
				$pcit
		);
}
/**
 * Trigger the kernel
 *
 * @return void
 */
function ossn_kernel_trigger() {
		if(KERNEL_STATUS === true) {
				OssnKernel::setINIT();
		}
}
/**
 * Cache flush
 *
 * @return void
 */
function ossn_kernel_cache_flushed(){
		OssnKernel::clearStorage();
}
ossn_register_callback('components', 'after:load', 'ossn_kernel_trigger');
ossn_register_callback('ossn', 'init', 'ossn_kernal_init');
ossn_register_system_sdk('Kernel', 'kernel_network_init');