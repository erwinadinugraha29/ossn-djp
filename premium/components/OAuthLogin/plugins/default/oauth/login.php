<?php
 $component = new OssnComponents;
 $settings  = $component->getSettings('OAuthLogin');
?>
<!-- http://localhost:8081/oauth/authorize?response_type=code&client_id=ossn&redirect_uri=http://127.0.0.1/premium/oauth_login/wordpress -->
<a href="<?=$settings->wp_consumer_authorization_url?>?client_id=<?=$settings->wp_consumer_key?>&response_type=code&redirect_url=<?=ossn_site_url('oauth_login/wordpress');?>" class="btn btn-block btn-social btn-wordpress btn-sm">
  <span class="fa fa-wordpress"></span>
  <?php echo ossn_print('oauth:login:with:wordpress');?>
</a>
