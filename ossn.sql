-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 27, 2021 at 03:27 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ossn`
--

-- --------------------------------------------------------

--
-- Table structure for table `ossn_annotations`
--

CREATE TABLE `ossn_annotations` (
  `id` bigint(20) NOT NULL,
  `owner_guid` bigint(20) NOT NULL,
  `subject_guid` bigint(20) NOT NULL,
  `type` varchar(30) NOT NULL,
  `time_created` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ossn_components`
--

CREATE TABLE `ossn_components` (
  `id` bigint(20) NOT NULL,
  `com_id` varchar(50) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ossn_components`
--

INSERT INTO `ossn_components` (`id`, `com_id`, `active`) VALUES
(1, 'OssnProfile', 1),
(2, 'OssnWall', 1),
(3, 'OssnComments', 1),
(4, 'OssnLikes', 1),
(5, 'OssnPhotos', 1),
(6, 'OssnNotifications', 1),
(7, 'OssnSearch', 1),
(8, 'OssnMessages', 1),
(9, 'OssnAds', 0),
(10, 'OssnGroups', 1),
(11, 'OssnSitePages', 1),
(12, 'OssnBlock', 1),
(13, 'OssnChat', 1),
(14, 'OssnPoke', 1),
(15, 'OssnInvite', 1),
(16, 'OssnEmbed', 1),
(17, 'OssnSmilies', 1),
(18, 'Kernel', 1),
(19, 'BanUser', 1),
(20, 'Birthdays', 1),
(21, 'Categories', 1),
(22, 'Censorship', 1),
(23, 'Dislike', 1),
(24, 'MobileLogin', 0),
(25, 'MultiUpload', 1),
(26, 'PhoneNumbers', 1),
(27, 'Sentiment', 1),
(28, 'SiteOffline', 1),
(29, 'Softlab24', 1),
(30, 'Styler', 1),
(31, 'Videos', 1),
(32, 'SocialLogin', 0),
(33, 'EMembers', 1),
(34, 'SharePost', 1),
(35, 'AccessCode', 0),
(36, 'CustomFields', 1),
(37, 'Moderator', 1),
(38, 'OssnSounds', 1),
(39, 'Hangout', 1),
(40, 'UserVerified', 1),
(41, 'LinkPreview', 1),
(42, 'Report', 1),
(43, 'HashTag', 1),
(44, 'OssnAutoPagination', 1),
(45, 'BusinessPage', 0),
(46, 'Polls', 1),
(47, 'Announcement', 1),
(48, 'Feedback', 0),
(49, 'FirstLogin', 1),
(50, 'GDPR', 1),
(51, 'GroupInvite', 1),
(52, 'Hangout', 1),
(53, 'PasswordValidation', 1),
(54, 'MenuBuilder', 1),
(55, 'Events', 1),
(56, 'PrivateNetwork', 0),
(57, 'OssnMessageTyping', 1),
(58, 'OssnRealTimeComments', 1),
(59, 'OssnPostBackground', 1),
(60, 'Stories', 1),
(61, 'RememberLogin', 0),
(64, 'OAuthLogin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ossn_entities`
--

CREATE TABLE `ossn_entities` (
  `guid` bigint(20) NOT NULL,
  `owner_guid` bigint(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `subtype` varchar(50) NOT NULL,
  `time_created` int(11) NOT NULL,
  `time_updated` int(11) DEFAULT NULL,
  `permission` int(11) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ossn_entities`
--

INSERT INTO `ossn_entities` (`guid`, `owner_guid`, `type`, `subtype`, `time_created`, `time_updated`, `permission`, `active`) VALUES
(1, 30, 'component', 'styler', 1454501038, 0, 2, 1),
(2, 1, 'object', 'field_name', 1513615709, 0, 2, 1),
(3, 1, 'object', 'field_type', 1513615709, 0, 2, 1),
(4, 1, 'object', 'placeholder', 1513615709, 0, 2, 1),
(5, 1, 'object', 'show_on_signup', 1513615709, 0, 2, 1),
(6, 1, 'object', 'is_required', 1513615709, 0, 2, 1),
(7, 1, 'object', 'show_on_about', 1513615709, 0, 2, 1),
(8, 1, 'object', 'show_label', 1513615709, 0, 2, 1),
(9, 2, 'object', 'field_name', 1513615709, 0, 2, 1),
(10, 2, 'object', 'field_type', 1513615709, 0, 2, 1),
(11, 2, 'object', 'placeholder', 1513615709, 0, 2, 1),
(12, 2, 'object', 'show_on_signup', 1513615709, 0, 2, 1),
(13, 2, 'object', 'is_required', 1513615709, 0, 2, 1),
(14, 2, 'object', 'show_on_about', 1513615709, 0, 2, 1),
(15, 2, 'object', 'show_label', 1513615709, 0, 2, 1),
(16, 2, 'object', 'field_options', 1513615709, 0, 2, 1),
(17, 36, 'component', 'init', 1513615709, 0, 2, 1),
(18, 31, 'component', 'upgrade', 1513615709, 0, 2, 1),
(19, 1, 'user', 'birthdate', 1621222477, 0, 2, 1),
(20, 1, 'user', 'gender', 1621222477, 0, 2, 1),
(21, 1, 'user', 'password_algorithm', 1621222477, 0, 2, 1),
(22, 1, 'user', 'cover_time', 1621222531, 0, 2, 1),
(23, 1, 'user', 'file:profile:photo', 1621222538, 0, 2, 1),
(24, 1, 'user', 'icon_time', 1621222538, 1621223017, 2, 1),
(25, 1, 'user', 'icon_guid', 1621222538, 1621223017, 2, 1),
(26, 3, 'object', 'item_type', 1621222538, 0, 2, 1),
(27, 3, 'object', 'item_guid', 1621222538, 0, 2, 1),
(28, 3, 'object', 'poster_guid', 1621222538, 0, 2, 1),
(29, 3, 'object', 'access', 1621222538, 0, 2, 1),
(30, 3, 'object', 'time_updated', 1621222538, 0, 2, 1),
(31, 3, 'object', 'linkPreview', 1621222539, 0, 2, 1),
(32, 1, 'user', 'file:profile:photo', 1621223017, 0, 2, 1),
(33, 4, 'object', 'item_type', 1621223017, 0, 2, 1),
(34, 4, 'object', 'item_guid', 1621223017, 0, 2, 1),
(35, 4, 'object', 'poster_guid', 1621223017, 0, 2, 1),
(36, 4, 'object', 'access', 1621223017, 0, 2, 1),
(37, 4, 'object', 'time_updated', 1621223017, 0, 2, 1),
(38, 4, 'object', 'linkPreview', 1621223018, 0, 2, 1),
(39, 5, 'object', 'item_type', 1621225459, 0, 2, 1),
(40, 5, 'object', 'item_guid', 1621225459, 0, 2, 1),
(41, 5, 'object', 'poster_guid', 1621225459, 0, 2, 1),
(42, 5, 'object', 'access', 1621225459, 0, 2, 1),
(43, 5, 'object', 'time_updated', 1621225459, 0, 2, 1),
(44, 5, 'object', 'sentiment', 1621225460, 0, 2, 1),
(45, 5, 'object', 'linkPreview', 1621225460, 0, 2, 1),
(46, 6, 'object', 'file:storyfile', 1621225475, 0, 2, 1),
(47, 7, 'object', 'file:storyfile', 1621225499, 0, 2, 1),
(48, 64, 'component', 'wp_consumer_authorization_url', 1621545674, 0, 2, 1),
(49, 64, 'component', 'wp_consumer_token_url', 1621545674, 0, 2, 1),
(50, 64, 'component', 'wp_consumer_key', 1621545674, 0, 2, 1),
(51, 64, 'component', 'wp_consumer_secret', 1621545674, 0, 2, 1),
(52, 64, 'component', 'wp_consumer_endpoint_url', 1621545674, 0, 2, 1),
(53, 2, 'user', 'password_algorithm', 1621555141, 0, 2, 1),
(54, 3, 'user', 'password_algorithm', 1621556094, 0, 2, 1),
(55, 8, 'object', 'file:storyfile', 1621556115, 0, 2, 1),
(56, 3, 'user', 'cover_time', 1621556375, 0, 2, 1),
(57, 3, 'user', 'file:profile:photo', 1621556393, 0, 2, 1),
(58, 3, 'user', 'icon_time', 1621556393, 1621556418, 2, 1),
(59, 3, 'user', 'icon_guid', 1621556393, 1621556418, 2, 1),
(60, 9, 'object', 'item_type', 1621556393, 0, 2, 1),
(61, 9, 'object', 'item_guid', 1621556393, 0, 2, 1),
(62, 9, 'object', 'poster_guid', 1621556393, 0, 2, 1),
(63, 9, 'object', 'access', 1621556393, 0, 2, 1),
(64, 9, 'object', 'time_updated', 1621556393, 0, 2, 1),
(65, 9, 'object', 'linkPreview', 1621556393, 0, 2, 1),
(66, 3, 'user', 'file:profile:photo', 1621556418, 0, 2, 1),
(67, 10, 'object', 'item_type', 1621556418, 0, 2, 1),
(68, 10, 'object', 'item_guid', 1621556418, 0, 2, 1),
(69, 10, 'object', 'poster_guid', 1621556418, 0, 2, 1),
(70, 10, 'object', 'access', 1621556418, 0, 2, 1),
(71, 10, 'object', 'time_updated', 1621556418, 0, 2, 1),
(72, 10, 'object', 'linkPreview', 1621556418, 0, 2, 1),
(73, 4, 'user', 'password_algorithm', 1621557211, 0, 2, 1),
(74, 11, 'object', 'item_type', 1621557323, 0, 2, 1),
(75, 11, 'object', 'item_guid', 1621557323, 0, 2, 1),
(76, 11, 'object', 'poster_guid', 1621557323, 0, 2, 1),
(77, 11, 'object', 'access', 1621557323, 0, 2, 1),
(78, 11, 'object', 'time_updated', 1621557323, 0, 2, 1),
(79, 11, 'object', 'sentiment', 1621557323, 0, 2, 1),
(80, 11, 'object', 'linkPreview', 1621557323, 0, 2, 1),
(81, 5, 'user', 'password_algorithm', 1621557497, 0, 2, 1),
(82, 12, 'object', 'item_type', 1621557512, 0, 2, 1),
(83, 12, 'object', 'item_guid', 1621557512, 0, 2, 1),
(84, 12, 'object', 'poster_guid', 1621557512, 0, 2, 1),
(85, 12, 'object', 'access', 1621557512, 0, 2, 1),
(86, 12, 'object', 'time_updated', 1621557512, 0, 2, 1),
(87, 12, 'object', 'sentiment', 1621557513, 0, 2, 1),
(88, 12, 'object', 'linkPreview', 1621557513, 0, 2, 1),
(89, 13, 'object', 'file:storyfile', 1621557522, 0, 2, 1),
(90, 5, 'user', 'cover_time', 1621557528, 0, 2, 1),
(91, 5, 'user', 'file:profile:photo', 1621557573, 0, 2, 1),
(92, 5, 'user', 'icon_time', 1621557573, 0, 2, 1),
(93, 5, 'user', 'icon_guid', 1621557573, 0, 2, 1),
(94, 14, 'object', 'item_type', 1621557573, 0, 2, 1),
(95, 14, 'object', 'item_guid', 1621557573, 0, 2, 1),
(96, 14, 'object', 'poster_guid', 1621557573, 0, 2, 1),
(97, 14, 'object', 'access', 1621557573, 0, 2, 1),
(98, 14, 'object', 'time_updated', 1621557573, 0, 2, 1),
(99, 14, 'object', 'linkPreview', 1621557574, 0, 2, 1),
(100, 1, 'user', 'theme_darkmode', 1621561194, 1621561249, 2, 1),
(101, 3, 'user', 'theme_darkmode', 1621562799, 1621565638, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ossn_entities_metadata`
--

CREATE TABLE `ossn_entities_metadata` (
  `id` bigint(20) NOT NULL,
  `guid` bigint(20) NOT NULL,
  `value` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ossn_entities_metadata`
--

INSERT INTO `ossn_entities_metadata` (`id`, `guid`, `value`) VALUES
(1, 1, 'blue'),
(2, 2, 'birthdate'),
(3, 3, 'text'),
(4, 4, 'birthdate'),
(5, 5, 'yes'),
(6, 6, 'yes'),
(7, 7, 'yes'),
(8, 8, 'no'),
(9, 9, 'gender'),
(10, 10, 'radio'),
(11, 11, 'gender'),
(12, 12, 'yes'),
(13, 13, 'yes'),
(14, 14, 'yes'),
(15, 15, 'no'),
(16, 16, '[\"male\",\"female\"]'),
(17, 17, '1'),
(18, 18, 'v5.2'),
(19, 19, '29/08/1995'),
(20, 20, 'male'),
(21, 21, 'bcrypt'),
(22, 22, '1621222531'),
(23, 23, 'profile/photo/3033db9183c56e803da277c6fdf3c8e6.jpg'),
(24, 24, '1621223017'),
(25, 25, '32'),
(26, 26, 'profile:photo'),
(27, 27, '23'),
(28, 28, '1'),
(29, 29, '2'),
(30, 30, '0'),
(31, 31, ''),
(32, 32, 'profile/photo/d851bcc2810573b12d502ce8d948fe48.jpg'),
(33, 33, 'profile:photo'),
(34, 34, '32'),
(35, 35, '1'),
(36, 36, '2'),
(37, 37, '0'),
(38, 38, ''),
(39, 39, ''),
(40, 40, ''),
(41, 41, '1'),
(42, 42, '2'),
(43, 43, '0'),
(44, 44, ''),
(45, 45, ''),
(46, 46, 'story/contents/54effe5dfb0b8b02d4f0429c41020aaf.jpg'),
(47, 47, 'story/contents/500a9325e18728740d2855404177db8a.jpg'),
(48, 48, 'http://localhost:8081/oauth/authorize'),
(49, 49, 'http://localhost:8081/oauth/token'),
(50, 50, 'ossn'),
(51, 51, 'ossn'),
(52, 52, 'http://localhost:8081/oauth/check_token'),
(53, 53, 'bcrypt'),
(54, 54, 'bcrypt'),
(55, 55, 'story/contents/7ba4d457417a12407f284b39e5a3cd7d.jpg'),
(56, 56, '1621556375'),
(57, 57, 'profile/photo/89783aae8ebb5e3ea538160d0e92cf14.jpg'),
(58, 58, '1621556418'),
(59, 59, '66'),
(60, 60, 'profile:photo'),
(61, 61, '57'),
(62, 62, '3'),
(63, 63, '2'),
(64, 64, '0'),
(65, 65, ''),
(66, 66, 'profile/photo/8adee490e215e5e2501c654944b96a6f.png'),
(67, 67, 'profile:photo'),
(68, 68, '66'),
(69, 69, '3'),
(70, 70, '2'),
(71, 71, '0'),
(72, 72, ''),
(73, 73, 'bcrypt'),
(74, 74, ''),
(75, 75, ''),
(76, 76, '4'),
(77, 77, '2'),
(78, 78, '0'),
(79, 79, ''),
(80, 80, ''),
(81, 81, 'bcrypt'),
(82, 82, ''),
(83, 83, ''),
(84, 84, '5'),
(85, 85, '2'),
(86, 86, '0'),
(87, 87, ''),
(88, 88, ''),
(89, 89, 'story/contents/327154b5eef889bf99a9a9c31a49947f.jpg'),
(90, 90, '1621557528'),
(91, 91, 'profile/photo/f790ab720a0554f68d4fa1b46065528d.jpg'),
(92, 92, '1621557573'),
(93, 93, '91'),
(94, 94, 'profile:photo'),
(95, 95, '91'),
(96, 96, '5'),
(97, 97, '2'),
(98, 98, '0'),
(99, 99, ''),
(100, 100, '0'),
(101, 101, '0');

-- --------------------------------------------------------

--
-- Table structure for table `ossn_likes`
--

CREATE TABLE `ossn_likes` (
  `id` bigint(20) NOT NULL,
  `subject_id` bigint(20) NOT NULL,
  `guid` bigint(20) NOT NULL,
  `type` varchar(30) NOT NULL,
  `subtype` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ossn_messages`
--

CREATE TABLE `ossn_messages` (
  `id` bigint(20) NOT NULL,
  `message_from` bigint(20) NOT NULL,
  `message_to` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `viewed` varchar(1) DEFAULT NULL,
  `time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ossn_notifications`
--

CREATE TABLE `ossn_notifications` (
  `guid` bigint(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `poster_guid` bigint(20) NOT NULL,
  `owner_guid` bigint(20) NOT NULL,
  `subject_guid` bigint(20) NOT NULL,
  `viewed` varchar(1) DEFAULT NULL,
  `time_created` int(11) NOT NULL,
  `item_guid` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ossn_object`
--

CREATE TABLE `ossn_object` (
  `guid` bigint(20) NOT NULL,
  `owner_guid` bigint(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `time_created` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` longtext NOT NULL,
  `subtype` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ossn_object`
--

INSERT INTO `ossn_object` (`guid`, `owner_guid`, `type`, `time_created`, `title`, `description`, `subtype`) VALUES
(1, 1, 'site', 1513615709, '', '', 'custom:fields'),
(2, 1, 'site', 1513615709, '', '', 'custom:fields'),
(3, 1, 'user', 1621222538, '', '{\"post\":\"null:data\"}', 'wall'),
(4, 1, 'user', 1621223017, '', '{\"post\":\"null:data\"}', 'wall'),
(5, 1, 'user', 1621225459, '', '{\"post\":\"aduh pusing\"}', 'wall'),
(6, 1, 'user', 1621225475, 'makan seblak atuh', '', 'story'),
(7, 1, 'user', 1621225499, 'hayuk ke angkasa', '', 'story'),
(8, 3, 'user', 1621556115, '', '', 'story'),
(9, 3, 'user', 1621556393, '', '{\"post\":\"null:data\"}', 'wall'),
(10, 3, 'user', 1621556418, '', '{\"post\":\"null:data\"}', 'wall'),
(11, 4, 'user', 1621557323, '', '{\"post\":\"hallo aku yan suseno\"}', 'wall'),
(12, 5, 'user', 1621557512, '', '{\"post\":\"Hallo akku moh faisal solehudin\"}', 'wall'),
(13, 5, 'user', 1621557522, '', '', 'story'),
(14, 5, 'user', 1621557573, '', '{\"post\":\"null:data\"}', 'wall'),
(15, 1, 'site', 1621565685, 'primary', '&lt;p&gt;Welcome to DJP Social Network&lt;/p&gt;', 'awesome:theme');

-- --------------------------------------------------------

--
-- Table structure for table `ossn_relationships`
--

CREATE TABLE `ossn_relationships` (
  `relation_id` bigint(20) NOT NULL,
  `relation_from` bigint(20) NOT NULL,
  `relation_to` bigint(20) NOT NULL,
  `type` varchar(30) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ossn_relationships`
--

INSERT INTO `ossn_relationships` (`relation_id`, `relation_from`, `relation_to`, `type`, `time`) VALUES
(1, 5, 3, 'friend:request', 1621557538);

-- --------------------------------------------------------

--
-- Table structure for table `ossn_site_settings`
--

CREATE TABLE `ossn_site_settings` (
  `setting_id` bigint(20) NOT NULL,
  `name` varchar(30) NOT NULL,
  `value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ossn_site_settings`
--

INSERT INTO `ossn_site_settings` (`setting_id`, `name`, `value`) VALUES
(1, 'theme', 'white'),
(2, 'site_name', 'ossn'),
(3, 'language', 'en'),
(4, 'cache', '1'),
(5, 'owner_email', 'ossn@pajak.go.id'),
(6, 'notification_email', 'ossn@pajak.go.id'),
(7, 'upgrades', '[\"1410545706.php\",\"1411396351.php\", \"1412353569.php\",\"1415553653.php\",\"1415819862.php\", \"1423419053.php\", \"1423419054.php\", \"1439295894.php\", \"1440716428.php\", \"1440867331.php\", \"1440603377.php\", \"1443202118.php\", \"1443211017.php\", \"1443545762.php\", \"1443617470.php\", \"1446311454.php\", \"1448807613.php\", \"1453676400.php\", \"1459411815.php\", \"1468010638.php\", \"1470127853.php\", \"1480759958.php\", \"1495366993.php\", \"1513524535.php\", \"1513603766.php\", \"1513783390.php\", \"1542223614.php\", \"1564080285.php\", \"1577836800.php\", \"1597058454.php\", \"1597090556.php\", \"1597734806.php\", \"1598389337.php\", \"1605286634.php\", \"1606063717.php\"]'),
(9, 'display_errors', 'on'),
(10, 'site_key', '33eb39b6'),
(11, 'last_cache', '1621567791'),
(12, 'site_version', '5.6'),
(13, 'com_white_theme_mode', 'litemode');

-- --------------------------------------------------------

--
-- Table structure for table `ossn_users`
--

CREATE TABLE `ossn_users` (
  `guid` bigint(20) NOT NULL,
  `type` text NOT NULL,
  `username` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `salt` varchar(8) NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `last_login` int(11) NOT NULL,
  `last_activity` int(11) NOT NULL,
  `activation` varchar(32) DEFAULT NULL,
  `time_created` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ossn_users`
--

INSERT INTO `ossn_users` (`guid`, `type`, `username`, `email`, `password`, `salt`, `first_name`, `last_name`, `last_login`, `last_activity`, `activation`, `time_created`) VALUES
(1, 'admin', 'admin', 'erwinadinugraha@gmail.com', '$2y$10$x/J/.B6yoqXpcFwvO4BzOerIRO7Rb8Z4BLU3NKgQBBIIimJhjfCN6', '44d03312', 'erwin', 'adinugraha', 1621564082, 1621567830, '', 1621222477),
(2, 'normal', 'MARKICOB', 'dwadawdawd@gmail.com', '$2y$10$tgCgk.FD363CsBp8rnwCy.bhYjVs39zF2KPXHTjUcJxfYFVYElaVm', '7c5dd5b2', 'ANJAYANI', 'UHUY', 1621555142, 1621555304, '', 1621555141),
(3, 'normal', 'erwin', 'dwadawdawda@gmail.com', '$2y$10$4.rFm48vHPIFlUUrdGJmy.TnKVI5HeD5nKlhopxWxbx1SfXgHsN4i', 'b7eaedff', 'Erwin', 'Adinugraha', 1621556094, 1621581788, '', 1621556094),
(4, 'normal', 'yansuseno', 'dwadawdawda1@gmail.com', '$2y$10$jJtgOsLc4tuJJm8lpGHg7uuLxYmn8nKcQCPVsMJEf4GP5BGCehc5W', 'fdb9c70e', 'Yan', 'Suseno', 1621557211, 1621599765, '', 1621557211),
(5, 'normal', 'mohical', 'mohical@pajak.go.id', '$2y$10$K5tWZB.XuRXmdVAh4252zeWXH8yhonPu1JCJDFgf6gwWxnUEWiJhW', '0f9aafd2', 'Moh', 'Faisal', 1621557497, 1621557618, '', 1621557497);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ossn_annotations`
--
ALTER TABLE `ossn_annotations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner_guid` (`owner_guid`),
  ADD KEY `subject_guid` (`subject_guid`),
  ADD KEY `time_created` (`time_created`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `ossn_components`
--
ALTER TABLE `ossn_components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ossn_entities`
--
ALTER TABLE `ossn_entities`
  ADD PRIMARY KEY (`guid`),
  ADD KEY `owner_guid` (`owner_guid`),
  ADD KEY `time_created` (`time_created`),
  ADD KEY `time_updated` (`time_updated`),
  ADD KEY `active` (`active`),
  ADD KEY `permission` (`permission`),
  ADD KEY `type` (`type`),
  ADD KEY `subtype` (`subtype`),
  ADD KEY `eky_ts` (`type`,`subtype`),
  ADD KEY `eky_tso` (`type`,`subtype`,`owner_guid`);

--
-- Indexes for table `ossn_entities_metadata`
--
ALTER TABLE `ossn_entities_metadata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `guid` (`guid`);
ALTER TABLE `ossn_entities_metadata` ADD FULLTEXT KEY `value` (`value`);

--
-- Indexes for table `ossn_likes`
--
ALTER TABLE `ossn_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subtype` (`subtype`);

--
-- Indexes for table `ossn_messages`
--
ALTER TABLE `ossn_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `message_to` (`message_to`),
  ADD KEY `message_from` (`message_from`);

--
-- Indexes for table `ossn_notifications`
--
ALTER TABLE `ossn_notifications`
  ADD PRIMARY KEY (`guid`),
  ADD KEY `poster_guid` (`poster_guid`),
  ADD KEY `owner_guid` (`owner_guid`),
  ADD KEY `subject_guid` (`subject_guid`),
  ADD KEY `time_created` (`time_created`),
  ADD KEY `item_guid` (`item_guid`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `ossn_object`
--
ALTER TABLE `ossn_object`
  ADD PRIMARY KEY (`guid`),
  ADD KEY `owner_guid` (`owner_guid`),
  ADD KEY `time_created` (`time_created`),
  ADD KEY `type` (`type`),
  ADD KEY `subtype` (`subtype`),
  ADD KEY `oky_ts` (`type`,`subtype`),
  ADD KEY `oky_tsg` (`type`,`subtype`,`guid`);

--
-- Indexes for table `ossn_relationships`
--
ALTER TABLE `ossn_relationships`
  ADD PRIMARY KEY (`relation_id`),
  ADD KEY `relation_to` (`relation_to`),
  ADD KEY `relation_from` (`relation_from`),
  ADD KEY `time` (`time`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `ossn_site_settings`
--
ALTER TABLE `ossn_site_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `ossn_users`
--
ALTER TABLE `ossn_users`
  ADD PRIMARY KEY (`guid`),
  ADD KEY `last_login` (`last_login`),
  ADD KEY `last_activity` (`last_activity`),
  ADD KEY `time_created` (`time_created`);
ALTER TABLE `ossn_users` ADD FULLTEXT KEY `type` (`type`);
ALTER TABLE `ossn_users` ADD FULLTEXT KEY `email` (`email`);
ALTER TABLE `ossn_users` ADD FULLTEXT KEY `first_name` (`first_name`);
ALTER TABLE `ossn_users` ADD FULLTEXT KEY `last_name` (`last_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ossn_annotations`
--
ALTER TABLE `ossn_annotations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ossn_components`
--
ALTER TABLE `ossn_components`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `ossn_entities`
--
ALTER TABLE `ossn_entities`
  MODIFY `guid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `ossn_entities_metadata`
--
ALTER TABLE `ossn_entities_metadata`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `ossn_likes`
--
ALTER TABLE `ossn_likes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ossn_messages`
--
ALTER TABLE `ossn_messages`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ossn_notifications`
--
ALTER TABLE `ossn_notifications`
  MODIFY `guid` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ossn_object`
--
ALTER TABLE `ossn_object`
  MODIFY `guid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `ossn_relationships`
--
ALTER TABLE `ossn_relationships`
  MODIFY `relation_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ossn_site_settings`
--
ALTER TABLE `ossn_site_settings`
  MODIFY `setting_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `ossn_users`
--
ALTER TABLE `ossn_users`
  MODIFY `guid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
